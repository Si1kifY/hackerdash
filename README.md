# HackerDash

## Version 1.0 - Released under GPL - Callum Silcock

HackerDash is a chrome extension written in AngularJS to give you quick access to posts on:

*   HackerNews

    *   Hot (default)

    *   +5\. Points

    *   +10\. Points

    *   +20\. Points

*   DesignerNews

*   Reddit

    *   /r/web_design (default)

    *   /r/userexperience

    *   /r/design

    *   /r/webdev