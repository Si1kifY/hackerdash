'use strict';

function updateClock() {
// Gets the current time
var now = new Date();

// Get the hours, minutes and seconds from the current time
var hours = now.getHours();
var minutes = now.getMinutes();
var seconds = now.getSeconds();

// Format hours, minutes and seconds
if (hours < 10) {
    hours = "0" + hours;
}
if (minutes < 10) {
    minutes = "0" + minutes;
}
if (seconds < 10) {
    seconds = "0" + seconds;
}

// Gets the element we want to inject the clock into
var elem = document.getElementById('time');

// Sets the elements inner HTML value to our clock data
elem.innerHTML = hours + ':' + minutes + ':' + seconds;
}

var App = angular.module('feedApp', []); 

App.controller("HNControl", ['$scope', 'FeedService', function ($scope, Feed) {
	$scope.loadFeed = function (e) {
		Feed.parseFeed($scope.feedSrc).then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;
		});
	}
	$scope.init = function(e) {
		Feed.parseFeed('https://news.ycombinator.com/rss').then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;
		});
	}
}]);

App.factory('FeedService', ['$http', function ($http) {
	return {
		parseFeed: function (url) {
			return $http.jsonp('https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=' + encodeURIComponent(url));
		}
	}
}]);

App.controller("RedditControl", ['$scope', 'FeedService', function ($scope, Feed) {
	$scope.loadFeed = function (e) {
		Feed.parseFeed($scope.feedSrc).then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;
		});
	}
	$scope.init = function(e) {
		Feed.parseFeed('http://www.reddit.com/r/web_design/.rss').then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;

		});
	}
}]);

App.controller("DNControl", ['$scope', 'FeedService', function ($scope, Feed) {
	$scope.loadFeed = function (e) {
		Feed.parseFeed($scope.feedSrc).then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;
		});
	}
	$scope.init = function(e) {
		Feed.parseFeed('https://news.layervault.com/?format=rss').then(function (res) {
			$scope.feeds = res.data.responseData.feed.entries;
		});
	}
}]);
$(document).ready(function(){

setInterval('updateClock()', 200);
});